import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@granite-elements/granite-bootstrap/granite-bootstrap.js';
import 'wc-validated-message/wc-validated-message.js';
import 'gtc-wc-accession/gtc-wc-accession.js';

class WcInvalidWurcsList extends PolymerElement {
  static get template() {
    return html`
<style include="granite-bootstrap">
/* shadow DOM styles go here */:

</style>

  <iron-ajax
    auto url="https://test.sparqlist.glycosmos.org/sparqlist/api/wurcs_select_invalid_wurcs_list?limit={{limit}}&offset={{offset}}"
    handle-as="json"
    last-response="{{sampleids}}"></iron-ajax>
    <!--  key => hashkey, wurcs -->

  <div class="table-responsive">
    <table class="table table-bordered">
      <thead>
        <tr>
          <th>Warning / Error message</th>
          <th>Accession</th>
          <th>WURCS</th>
        </tr>
      </thead>
      <tbody>
        <template is="dom-repeat" items="{{sampleids}}">
          <tr>
            <td><wc-validated-message hashed_text="{{item.hashkey}}"></wc-validated-message></td>
            <td><gtc-wc-accession wurcs="{{item.wurcs}}" hashed_text="{{item.hashkey}}"></gtc-wc-accession></td>
            <td>[[item.wurcs]]</td>
          </tr>
        </template>
      </tbody>
    </table>
  </div>
   `;
  }
  constructor() {
    super();
  }
  static get properties() {
    return {
      sampleids: {
        notify: true,
        type: Object,
        value: function() {
          return new Object();
        }
      },
      limit: {
        notify: true,
        type: String
      },
      offset: {
        notify: true,
        type: String
      },
      hashkey: {
        notify: true,
        type: String
      },
      wurcs: {
        notify: true,
        type: String
      }
    };
  }

  _handleAjaxPostResponse(e) {
    console.log(e);
  }
  _handleAjaxPostError(e) {
    console.log('error: ' + e);
  }
}

customElements.define('wc-invalid-wurcs-list', WcInvalidWurcsList);
